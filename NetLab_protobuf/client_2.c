#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include "netmsg.pb-c.h"

#define MSG_MAX 15

struct Msg
{
    int waitTime;
    int strLeng;
    char *str;
};

void GetMsg(int *arg)
{
    int *clientSock = (int *) arg;
    unsigned int msg_len;
    uint8_t buf[MSG_MAX];
    struct Msg msg;
    NetMsg *pbMsg;
     
    if (recv(*clientSock, &msg_len, sizeof(unsigned int), 0) == -1)
    {
        perror("Can`t recv string");
	sleep(3);
        exit(EXIT_FAILURE);
    }

    printf("leng is %i\n", msg_len);

    if (recv(*clientSock, buf, msg_len, 0) == -1)
    {
        perror("Can`t recv string");
	sleep(3);
        exit(EXIT_FAILURE);
    }

    pbMsg = net_msg__unpack(NULL, msg_len, buf);	
    if (pbMsg == NULL)
    {
    	perror("error unpacking incoming message");
	sleep(3);
    	exit(1);
    }

    msg.waitTime = pbMsg->waittime;
    msg.strLeng = pbMsg->strleng;
    msg.str = pbMsg->str;

    printf("time: %i, leng: %i, string: %s\n", msg.waitTime, msg.strLeng, msg.str);

    net_msg__free_unpacked(pbMsg, NULL);

    sleep(msg.waitTime);
}

int main()
{
    sleep(1);
    int tcpServerSock, udpServerSock;
    int retVal;
    unsigned int udpAddrSize, clientAddrSize;
    struct sockaddr_in tcpSockAddr, udpSockAddr, clientAddr;
    char recvBuf[MSG_MAX];

    printf(" !! CLIENT 2 !! \n");

    udpServerSock = socket(AF_INET, SOCK_DGRAM, 0);
    if (setsockopt(udpServerSock, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    udpSockAddr.sin_family = AF_INET;
    udpSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    udpSockAddr.sin_port = htons(3001);
    udpAddrSize = sizeof(udpSockAddr);
    clientAddrSize = sizeof(clientAddr);

    if(bind(udpServerSock, (struct sockaddr*)&udpSockAddr, udpAddrSize) == -1)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for 'gsm' msg...\n");

    while (strcmp(recvBuf, "STP") != 0)
    {
        retVal = recvfrom(udpServerSock, recvBuf, sizeof(recvBuf), 0, (struct sockaddr *)&clientAddr, &clientAddrSize);
	if (retVal == -1)
	{
        	perror("recvfrom err");
		sleep(3);
        	exit(EXIT_FAILURE);
   	}

        printf("Got msg: %s\n", recvBuf);
        if (strcmp(recvBuf, "gsm") != 0) // сделать везде do - while
            continue;

        printf("Sending request...\n");

	clientAddr.sin_port = htons(3011);
        tcpServerSock = socket(AF_INET, SOCK_STREAM, 0);
        if (connect(tcpServerSock, (struct sockaddr *) &clientAddr, clientAddrSize) == -1) {
            perror("can`t connect");
	    sleep(3);
            exit(EXIT_FAILURE);
        }
	printf("Got connection...\n");

        GetMsg(&tcpServerSock);
        close(tcpServerSock);
    }

    close(udpServerSock);
    close(tcpServerSock);

    return 0;
}
