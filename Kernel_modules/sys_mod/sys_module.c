#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sysfs.h>
#include <linux/cdev.h>
#include <linux/parport.h>
#include <linux/uaccess.h>
#include <linux/pci.h>
#include <linux/version.h>

MODULE_AUTHOR("CODEGOD");
MODULE_LICENSE("GPL");
 
#define BUF_SIZE 100
static char msg_buf[BUF_SIZE] = "HI THERE!";

 
static ssize_t test_show(struct class *class, struct class_attribute *attr, char *buf) 
{
	strcpy(buf, msg_buf);
	//printk(KERN_INFO "read %d\n", strlen(buf));
	return strlen(buf);
}
 
static ssize_t test_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count) 
{
	//printk(KERN_INFO "write %d\n", count);
	int len = count;
	if(len > BUF_SIZE)
		len = BUF_SIZE - 1;
	strncpy(msg_buf, buf, len - 1);
	buf_msg[len] = '\0';
	return count;
}

CLASS_ATTR_RW(test);
 
static struct class *my_class;
 
static int __init sys_init(void) 
{
	int res;
	my_class = class_create(THIS_MODULE, "test_class");
	if(IS_ERR(my_class)) 
		printk("bad class create");
	res = class_create_file(my_class, &class_attr_test);
	printk(KERN_INFO "test module initialized");
	return 0;
}
 
static void __exit sys_exit(void)
{
	class_remove_file(my_class, &class_attr_test);
	class_destroy(my_class);
}
 
module_init(sys_init);
module_exit(sys_exit);
