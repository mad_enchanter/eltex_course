#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR("CODEGOD");

#define BUF_SIZE 100
#define PROC_FILE_NAME "test_proc"

static char msg_buffer[BUF_SIZE] = "HI THERE!";

struct proc_dir_entry *proc_file;

static ssize_t module_read(struct file *filp, char *buf, size_t len, loff_t *offset)
{
	static int finished = 0;
	int i;
	
	if(finished)
	{
		finished = 0;
		return 0;
	}
	
	for(i = 0; i < BUF_SIZE && msg_buffer[i]; i++)
		put_user(msg_buffer[i], buf + i);
	
	//copy_to_user();
	
	finished = 1;
	
	return i;
}

static ssize_t module_write(struct file *filp, const char *buf, size_t len, loff_t *offset)
{
	int i = 0;

	for(i = 0; i < len && i < BUF_SIZE; i++)
		get_user(msg_buffer[i], buf + i);

	msg_buffer[i] = '\0';
	return i;
}

static const struct file_operations file_ops = {
	.owner = THIS_MODULE,
	.read = module_read,
	.write = module_write
};

static int __init proc_init(void)
{
	int ret_val = 0;
	proc_file = proc_create(PROC_FILE_NAME, S_IFREG | S_IRUGO | S_IWUGO, NULL, &file_ops);
	
	if (proc_file == NULL) 
	{
    	ret_val = -ENOMEM;
    	remove_proc_entry(PROC_FILE_NAME, NULL);
    	printk(KERN_INFO "Error: Could not initialize /proc/test_proc\n");
  	}
	
	return ret_val;
}

static void __exit proc_exit(void)
{
	remove_proc_entry(PROC_FILE_NAME, NULL);
}

module_init(proc_init);
module_exit(proc_exit);
