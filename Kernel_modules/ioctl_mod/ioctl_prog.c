#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include "ioctl_module.h"

void ioctl_set_msg(int fd, char *msg)
{
    int ret_val;

    ret_val = ioctl(fd, IOCTL_SET_MSG, msg);
    if(ret_val < 0)
    {
        printf("can`t set msg, error:%d\n", ret_val);
        exit(EXIT_FAILURE);
    }
}

void ioctl_get_msg(int fd, char *msg)
{
    int ret_val;

    ret_val = ioctl(fd, IOCTL_GET_MSG, msg);
    if(ret_val < 0)
    {
        printf("can`t get msg, error:%d\n", ret_val);
        exit(EXIT_FAILURE);
    }
    //printf("got msg: %s\n", msg);
}

int main(int argc, char const *argv[])
{
    int fd, ex = 0, choise;
    char msg[100];
    fd = open(DEVICE_PATH, O_RDWR);
    if(fd < 0)
    {
        printf("can`t open device file\n");
        exit(EXIT_FAILURE);
    }

    while(ex == 0)
    {
    	printf("Press 1 to get msg\n");
    	printf("Press 2 to set msg\n");
    	printf("Press 3 to exit\n");
        scanf("%i", &choise);
        switch (choise)
        {
        case 1:
            ioctl_get_msg(fd, msg);
            printf("got msg: %s\n", msg);
            break;

        case 2:
            printf("Enter new msg:");
            scanf("%s",msg);
            ioctl_set_msg(fd, msg);
            system("clear");
            break;

        case 3:
            ex = 1;
            break;
        
        default:
            system("clear");
            printf("Try again\n");
            break;
        }
    }

    return 0;
}
