#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include "ioctl_module.h"

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR("CODEGOD");

#define BUF_SIZE 100

static int is_device_open = 0;
static char msg_buffer[BUF_SIZE] = "HI THERE!\n";

struct proc_dir_entry *proc_file;

static ssize_t device_read(struct file *filp, char *buf, size_t len, loff_t *offset)
{
	static int finished = 0;
	int i;
	
	if(finished)
	{
		finished = 0;
		return 0;
	}
	
	for(i = 0; i < BUF_SIZE && msg_buffer[i]; i++)
		put_user(msg_buffer[i], buf + i);
		
	if(i < BUF_SIZE)
		put_user('\0', buf + i);
	
	finished = 1;
	
	return i;
}

static ssize_t device_write(struct file *filp, const char *buf, size_t len, loff_t *offset)
{
	int i = 0;

	for(i = 0; i < len && i < BUF_SIZE; i++)
		get_user(msg_buffer[i], buf + i);

	msg_buffer[i] = '\0';
	return i;
}

static int device_open(struct inode *inode, struct file *file)
{
  	if (is_device_open)
    	return -EBUSY;
 	is_device_open++;
 	try_module_get(THIS_MODULE);

  	return 0;
}

static int device_release(struct inode *inode, struct file *file)
{
  	is_device_open--;                
  	module_put(THIS_MODULE);
  	return 0;
}

static long device_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
	int i;
  	char *temp;
  	char ch;

  	switch (ioctl_num) 
  	{
  	case IOCTL_SET_MSG:
    	temp = (char *)ioctl_param; // берем указатель на сообщение из userspace
    	get_user(ch, temp); 
    	for (i = 0; ch && i < BUF_SIZE; i++, temp++) // находим длинну сообщения 
      		get_user(ch, temp);
    	device_write(file, (char *)ioctl_param, i, 0);
    	break;

  	case IOCTL_GET_MSG:
    	i = device_read(file, (char *)ioctl_param, BUF_SIZE, 0);
    	//put_user('\n', (char *)ioctl_param + i);
    	//put_user('\0', (char *)ioctl_param + i + 1);
    	break;
    }

  	return 0;
}

static const struct file_operations file_ops = {
	.owner = THIS_MODULE,
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release,
	.unlocked_ioctl = device_ioctl
};

static int __init ioctl_init(void)
{
	int ret_val = 0;
	
	ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &file_ops);
	
	if(ret_val < 0)
	{
		printk("Registering the character device failed with %d\n", MAJOR_NUM);
    	return ret_val;
	}
	
	printk("<1>I was assigned major number %d.  To talk to\n", MAJOR_NUM);
  	printk("<1>the driver, create a dev file with\n");
  	printk("'mknod /dev/mydev c %d 0'.\n", MAJOR_NUM);
  	printk("<1>Try various minor numbers.  Try to cat and echo to\n");
  	printk("the device file.\n");
  	printk("<1>Remove the device file and module when done.\n");
	
	return ret_val;
}

static void __exit ioctl_exit(void)
{
	unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
}

module_init(ioctl_init);
module_exit(ioctl_exit);
