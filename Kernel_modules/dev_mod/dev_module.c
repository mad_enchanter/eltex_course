#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR("CODEGOD");

#define BUF_SIZE 100
#define DEVICE_NAME "mydev"

static int major;
static int is_device_open = 0;

static char msg_buffer[BUF_SIZE] = "HI THERE!\n";

struct proc_dir_entry *proc_file;

static ssize_t device_read(struct file *filp, char *buf, size_t len, loff_t *offset)
{
	static int finished = 0;
	int i;
	
	if(finished)
	{
		finished = 0;
		return 0;
	}
	
	for(i = 0; i < BUF_SIZE && msg_buffer[i]; i++)
		put_user(msg_buffer[i], buf + i);
	
	finished = 1;
	
	return i;
}

static ssize_t device_write(struct file *filp, const char *buf, size_t len, loff_t *offset)
{
	int i = 0;

	for(i = 0; i < len && i < BUF_SIZE; i++)
		get_user(msg_buffer[i], buf + i);

	msg_buffer[i] = '\0';
	return i;
}

static int device_open(struct inode *inode, struct file *file)
{
  	if (is_device_open)
    	return -EBUSY;
 	is_device_open++;
 	try_module_get(THIS_MODULE);

  	return 0;
}

static int device_release(struct inode *inode, struct file *file)
{
  	is_device_open--;                
  	module_put(THIS_MODULE);
  	return 0;
}

static const struct file_operations file_ops = {
	.owner = THIS_MODULE,
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};

static int __init proc_init(void)
{
	int ret_val = 0;
	
	major = register_chrdev(0, DEVICE_NAME, &file_ops);
	
	if(major < 0)
	{
		printk("Registering the character device failed with %d\n", major);
    	return major;
	}
	
	printk("<1>I was assigned major number %d.  To talk to\n", major);
  	printk("<1>the driver, create a dev file with\n");
  	printk("'mknod /dev/mydev c %d 0'.\n", major);
  	printk("<1>Try various minor numbers.  Try to cat and echo to\n");
  	printk("the device file.\n");
  	printk("<1>Remove the device file and module when done.\n");
	
	return ret_val;
}

static void __exit proc_exit(void)
{
	unregister_chrdev(major, DEVICE_NAME);
}

module_init(proc_init);
module_exit(proc_exit);
