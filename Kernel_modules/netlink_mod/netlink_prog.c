#include <linux/netlink.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#define NETLINK_USER 31
#define MAX_PAYLOAD 1024 

// если они не глобальные, не работает
struct nlmsghdr *nlh = NULL;
struct iovec iov;
struct msghdr msg;


int main()
{
	int sock, ret_val;
	struct sockaddr_nl src_addr, dest_addr;
    sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if (sock < 0)
	{
    	printf("can`t create socket\n");
    	exit(EXIT_FAILURE);
    }

    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid(); 

    bind(sock, (struct sockaddr *)&src_addr, sizeof(src_addr));
    if(ret_val < 0)
    {
        perror("Can`t bind");
        exit(EXIT_FAILURE);
    }

    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0; // for Linux Kernel always 0
    dest_addr.nl_groups = 0; // for unicast it`s 0

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;

    strcpy(NLMSG_DATA(nlh), "Hello from userspace");

    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&dest_addr;  // ????
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    printf("Sending message to kernel\n");
    sendmsg(sock, &msg, 0);
    printf("Waiting for message from kernel\n");

    recvmsg(sock, &msg, 0);
    printf("Received message payload: %s\n", NLMSG_DATA(nlh));
    
    close(sock);
    return 0;
}

/*
  NLMSG_SPACE - Возвращает размер, который займут данные указанной длины в пакете netlink
  NLMSG_DATA - Возвращает указатель на данные, связанные с переданным заголовком nlmsghdr

struct sockaddr_nl
{
	sa_family_t nl_family; 	// семейство протоколов - всегда AF_NETLINK
	unsigned short nl_pad; 	// поле всегда заполнено нулями
	pid_t nl_pid; 			// идентификатор процесса
	__u32 nl_groups; 		// маска групп получателей/отправителей
};

struct nlmsghdr
{
	__u32 nlmsg_len;	// размер сообщения, с учетом заголовка
	__u16 nlmsg_type; 	// тип содержимого сообщения (об этом ниже)
	__u16 nlmsg_flags;	// различные флаги сообщения
	__u32 nlmsg_seq; 	// порядковый номер сообщения
	__u32 nlmsg_pid; 	// идентификатор процесса (PID), отославшего сообщение
};

struct iovec
{
	void *iov_base;     	// буфер данных
	__kernel_size_t iov_len;  // размер данных
};

Эта структура служит хранилищем полезных данных, передаваемых через сокеты netlink. 
Полю iov_base присваивается указатель на байтовый массив. 
Именно в этот байтовый массив будут записаны данные сообщения.

struct msghdr {
    void   *msg_name;   // адрес клиента (имя сокета)
    int     	msg_namelen;    // длина адреса
    struct iovec *msg_iov;    // указатель на блок данных
    __kernel_size_t msg_iovlen; // количество блоков данных
    void    *msg_control;  // магическое число для протокола, не используется в данных случаях
    __kernel_size_t msg_controllen; // длина предидущего поля данных
    unsigned  msg_flags;	// флаги сообщения
};

Эта структура непосредственно передается через сокет. Она содержит в себе указатель на блок полезных данных, 
количество данных блоков, а так же ряд дополнительных флагов и полей, пришедших, по большей части, с платформы BSD.
*/
