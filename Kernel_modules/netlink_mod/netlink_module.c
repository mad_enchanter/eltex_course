#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/netlink.h>
#include <linux/skbuff.h> 
#include <net/sock.h> 
#define NETLINK_USER 31

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR("CODEGOD");

#define BUF_SIZE 100

static char msg_buffer[BUF_SIZE] = "HI THERE!";
struct sock *nl_sock = NULL; 

static void nl_recv_msg(struct sk_buff *skb)
{
	int pid, msg_size, ret_val;
	struct nlmsghdr *nlh; // header of msg
    struct sk_buff *skb_out; //sock buffer

    msg_size = strlen(msg_buffer);

    nlh = (struct nlmsghdr *)skb->data;
    printk(KERN_INFO "Netlink received msg payload:%s\n", (char *)nlmsg_data(nlh));
    pid = nlh->nlmsg_pid; // pid of sending process 

    skb_out = nlmsg_new(msg_size, 0);
    if (!skb_out) {
        printk(KERN_ERR "Failed to allocate new skb\n");
        return;
    }

    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0); // returns header of msg in buf (????????)
    NETLINK_CB(skb_out).dst_group = 0; // not in mcast group (??????????)
    strncpy(nlmsg_data(nlh), msg_buffer, msg_size); // copy payload in buf

    ret_val = nlmsg_unicast(nl_sock, skb_out, pid);
    if (ret_val < 0)
        printk(KERN_INFO "Error while sending back to user\n");
}

static int __init proc_init(void)
{
	int ret_val = 0;
	struct netlink_kernel_cfg cfg = {
        .input = nl_recv_msg
    };

    nl_sock = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);
    if (!nl_sock) {
        printk(KERN_ALERT "Error creating socket.\n");
        return -10;
    }
    
	printk(KERN_INFO "socket was created successfully\n");
	return ret_val;
}

static void __exit proc_exit(void)
{
	netlink_kernel_release(nl_sock);
}

module_init(proc_init);
module_exit(proc_exit);

/*
 * static inline void *nlmsg_data(const struct nlmsghdr *nlh)
 * nlmsg_data - head of message payload (видимо начало поля с данными)
 * @nlh: netlink message header
 */
 
 /*
 * static inline struct nlmsghdr *nlmsg_put(struct sk_buff *skb, u32 portid, u32 seq,
 *					 int type, int payload, int flags)
 * nlmsg_put - Add a new netlink message to an skb
 * @skb: socket buffer to store message in
 * @portid: netlink PORTID of requesting application
 * @seq: sequence number of message
 * @type: message type
 * @payload: length of message payload
 * @flags: message flags
 *
 * Returns NULL if the tailroom of the skb is insufficient to store
 * the message header and payload.
 */
 
/*
 * static inline struct sk_buff *nlmsg_new(size_t payload, gfp_t flags)
 * nlmsg_new - Allocate a new netlink message
 * @payload: size of the message payload
 * @flags: the type of memory to allocate.
 *
 * Use NLMSG_DEFAULT_SIZE if the size of the payload isn't known
 * and a good default is needed.
 */
 
 /*
 * static inline int nlmsg_unicast(struct sock *sk, struct sk_buff *skb, u32 portid)
 * nlmsg_unicast - unicast a netlink message
 * @sk: netlink socket to spread message to
 * @skb: netlink message as socket buffer
 * @portid: netlink portid of the destination socket
 */

/*
struct nlmsghdr
{
	__u32 nlmsg_len;	// размер сообщения, с учетом заголовка
	__u16 nlmsg_type; 	// тип содержимого сообщения 
	__u16 nlmsg_flags;	// различные флаги сообщения
	__u32 nlmsg_seq; 	// порядковый номер сообщения
	__u32 nlmsg_pid; 	// идентификатор процесса (PID), отославшего сообщение
};
*/
