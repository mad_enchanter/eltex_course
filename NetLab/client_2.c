#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>

#define MSG_MAX 10

void GetMsg(int *clientSock)
{
    int recvBuf[2];
    char *strBuf;

    recv(*clientSock, &recvBuf, sizeof(recvBuf), 0);   // recv header
    strBuf = malloc(sizeof(char) * recvBuf[1]);
    if(recv(*clientSock, strBuf, sizeof(char) * recvBuf[1], 0) == -1)        // recv random string
    {
        perror("Can`t recv string");
        exit(EXIT_FAILURE);
    }
    printf("time: %i, leng: %i, string: %s\n", recvBuf[0], recvBuf[1], strBuf);
    printf("gonna sleep for %i seconds...\n", recvBuf[0]);
    sleep(recvBuf[0]);
}

int main()
{
    int tcpServerSock, udpServerSock;
    int retVal;
    unsigned int udpAddrSize, clientAddrSize;
    struct sockaddr_in tcpSockAddr, udpSockAddr, clientAddr;
    char recvBuf[MSG_MAX];

    printf(" !! CLIENT 2 !! \n");

    udpServerSock = socket(AF_INET, SOCK_DGRAM, 0);
    if (setsockopt(udpServerSock, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    udpSockAddr.sin_family = AF_INET;
    udpSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    udpSockAddr.sin_port = htons(3001);
    udpAddrSize = sizeof(udpSockAddr);
    clientAddrSize = sizeof(clientAddr);

    if(bind(udpServerSock, (struct sockaddr*)&udpSockAddr, udpAddrSize) == -1)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for 'gsm' msg...\n");

    while (strcmp(recvBuf, "STP") != 0)
    {
        retVal = recvfrom(udpServerSock, recvBuf, sizeof(recvBuf), 0, (struct sockaddr *)&clientAddr, &clientAddrSize);
	if (retVal == -1)
	{
        	perror("recvfrom err");
        	exit(EXIT_FAILURE);
   	}

        printf("Got msg: %s\n", recvBuf);
        if (strcmp(recvBuf, "gsm") != 0) // сделать везде do - while
            continue;

        printf("Sending request...\n");

	clientAddr.sin_port = htons(3011);
        tcpServerSock = socket(AF_INET, SOCK_STREAM, 0);
        if (connect(tcpServerSock, (struct sockaddr *) &clientAddr, clientAddrSize) == -1) {
            perror("can`t connect");
            exit(EXIT_FAILURE);
        }
	printf("Got connection...\n");

        GetMsg(&tcpServerSock);
        close(tcpServerSock);
    }

    close(udpServerSock);
    close(tcpServerSock);

    return 0;
}
