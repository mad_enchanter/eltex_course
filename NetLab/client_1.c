#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>

#define MAX_WAIT_TIME 10
#define MSG_MAX 10

struct Msg
{
    int waitTime;
    int strLeng;
    char *str;
};

void GenerateMsg(struct Msg *msg)
{
    msg->waitTime = rand() % MAX_WAIT_TIME;
    msg->strLeng = rand() % MSG_MAX;

    msg->str = malloc(sizeof(char) * msg->strLeng);

    for (int i = 0; i < msg->strLeng; ++i)
    {
        msg->str[i] = 97 + rand() % 25;
    }

    msg->strLeng++;
    msg->str[msg->strLeng - 1] = '\0';
}

int main()
{
    int tcpServerSock, udpServerSock;
    unsigned int udpAddrSize, clientAddrSize;
    struct sockaddr_in tcpSockAddr, udpSockAddr, clientAddr;
    struct Msg msg;
    char recvBuf[MSG_MAX];

    srand(getpid());
    printf("!!! CLIENT 1 !!!\n");

    udpServerSock = socket(AF_INET, SOCK_DGRAM, 0);

    if (udpServerSock == -1)
    {
        perror("can`t set socket");
	sleep(3);
        exit(EXIT_FAILURE);
    }

    if (setsockopt(udpServerSock, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
	sleep(3);
        exit(EXIT_FAILURE);
    }
	
    udpSockAddr.sin_family = AF_INET;
    udpSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    udpSockAddr.sin_port = htons(3007);
    udpAddrSize = sizeof(udpSockAddr);
    clientAddrSize = sizeof(clientAddr);

    if(bind(udpServerSock, (struct sockaddr*)&udpSockAddr, udpAddrSize) == -1)
    {
        perror("bind failed");
	sleep(3);
        exit(EXIT_FAILURE);
    }

    sleep(1);
    printf("Waiting for 'wfm' msg...\n");

    while(strcmp(recvBuf, "STP") != 0)
    {
        if(recvfrom(udpServerSock, recvBuf, sizeof(recvBuf), 0, (struct sockaddr*)&clientAddr, &clientAddrSize) == -1)
	{
            perror("can`t recvfrom");
	    sleep(3);
            exit(EXIT_FAILURE);
	}
	
        printf("Got msg: %s\n", recvBuf);
        if(strcmp(recvBuf, "wfm") != 0) // игнорим остальные сообщения
            continue;

        printf("Sending request...\n");
	
	clientAddr.sin_port = htons(3009);
        tcpServerSock = socket(AF_INET, SOCK_STREAM, 0);
        if(connect(tcpServerSock, (struct sockaddr*)&clientAddr, clientAddrSize) == -1)
        {
            perror("can`t connect");
	    sleep(3);
            exit(EXIT_FAILURE);
        }
        printf("Got connection...\n");

        GenerateMsg(&msg);
        printf("time: %i, leng: %i, string: %s\n", msg.waitTime, msg.strLeng, msg.str);

        if(send(tcpServerSock, &msg, sizeof(int) * 2, 0) == -1) // отправляем только первые 2 поля
        {
            perror("can`t send");
	    sleep(3);
            exit(EXIT_FAILURE);
        }

        if(send(tcpServerSock, msg.str, msg.strLeng, 0) == -1)
        {
            perror("can`t send");
	    sleep(3);
            exit(EXIT_FAILURE);
        }
        close(tcpServerSock);
        printf("gonna sleep for %i sec...\n", msg.waitTime);
        sleep(msg.waitTime);
    }

    printf("client stoped...\n");
    close(udpServerSock);
    close(tcpServerSock);

    return 0;
}
