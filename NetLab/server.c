#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/queue.h>
#include <signal.h>

#define MAX_WAIT_TIME 5
#define MSG_MAX 10
#define QUEUE_MAX 3
#define WAIT_MSG_DELAY 3
#define GOT_MSG_DELAY 3

struct Msg {
    int waitTime;
    int strLeng;
    char *str;
};

struct QueueElem {
    struct Msg data;
    struct QueueElem *next;
};

int stopServer = 0;
int stop1 = 0;
int stop2 = 1;
int cureQueueSize = 0;

int listenSock1, listenSock2;
int tcpClientSock1, tcpClientSock2;

struct QueueElem *queueHead;
struct QueueElem *queueTail;

pthread_mutex_t lockAdd;
pthread_mutex_t lockGet;

void StopThread(int signo)
{
    pthread_exit(NULL);
}

int AddInQueue(struct Msg *newMsg) {
    if (cureQueueSize + 1 > QUEUE_MAX)
        return -1;

    if (cureQueueSize == 0) {
        queueTail = malloc(sizeof(struct QueueElem));
        queueTail->data = *newMsg;
        queueTail->next = NULL;
        queueHead = queueTail;
    }

    else {
        struct QueueElem *newElem = malloc(sizeof(struct QueueElem));
        newElem->data = *newMsg;
        newElem->next = NULL;
        queueTail->next = newElem;
        queueTail = newElem;
    }

    cureQueueSize++;
    if(cureQueueSize == QUEUE_MAX)  // enable block type 1
    {	
	printf("Queue is full now!\n");	
        stop1 = 1;
    }

    if(stop2 == 1) // disable block type 2
        stop2 = 0;

    return 0;
}

int TakeFromQueue(struct Msg *msgBuf) {
    if(cureQueueSize <= 0)
        return -1;

    *msgBuf = queueHead->data;

    if (cureQueueSize > 1)
        queueHead = queueHead->next;

    else
        queueHead = NULL;

    cureQueueSize--;

    if(cureQueueSize == 0) // enable block type 2
    {	
	printf("Queue is empty now!\n");	
        stop2 = 1;
    }

    if(stop1 == 1) // disable block type 1
        stop1 = 0;

    return 0;
}

void ShowQueue() {
    struct QueueElem *temp = queueHead;
    while (temp != NULL) {
        printf("time: %i, leng: %i, string: %s\n", temp->data.waitTime, temp->data.strLeng, temp->data.str);
        temp = temp->next;
    }
}

void *SendWaitMsg(void *arg) {

    int brcastSock1;
    struct sockaddr_in brcastAddr1;
    char brMsg[4];
    strcpy(brMsg, "wfm");
    brMsg[3] = '\0';

    brcastSock1 = socket(AF_INET, SOCK_DGRAM, 0);
    int broadcastEnable = 1;
    int ret = setsockopt(brcastSock1, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));

    brcastAddr1.sin_family = AF_INET;
    brcastAddr1.sin_addr.s_addr = htonl(INADDR_BROADCAST);//inet_addr("192.168.43.94");
    brcastAddr1.sin_port = htons(3007);
    unsigned int servAddrSize = sizeof(brcastAddr1);

    while (stopServer == 0)
    {
        if (sendto(brcastSock1, brMsg, sizeof(brMsg), 0, (struct sockaddr *) &brcastAddr1, servAddrSize) == -1) {
            perror("can`t send msg type 1");
            exit(EXIT_FAILURE);
        }

        printf("Send msg type 1\n");
        sleep(WAIT_MSG_DELAY);
        while(stop1 != 0 && stopServer == 0);
    }

    //char *stopMsg = "STOP";
    strcpy(brMsg, "STP");
    if (sendto(brcastSock1, brMsg, sizeof(brMsg), 0, (struct sockaddr *) &brcastAddr1, servAddrSize) == -1) {
        perror("can`t send msg type 1");
        exit(EXIT_FAILURE);
    }
    printf("Send STOP msg to clients type 1\n");

    pthread_exit(NULL);
}

void *SendGotMsg(void *arg) {
    int brcastSock;
    struct sockaddr_in brcastAddr;
    char brMsg[4];
    strcpy(brMsg, "gsm");
    brMsg[3] = '\0';

    brcastSock = socket(AF_INET, SOCK_DGRAM, 0);
    int broadcastEnable = 1;
    int ret = setsockopt(brcastSock, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));

    brcastAddr.sin_family = AF_INET;
    brcastAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST); // inet_addr("192.168.43.94");
    brcastAddr.sin_port = htons(3001);
    unsigned int servAddrSize = sizeof(brcastAddr);

    while(stop2 != 0 && stopServer == 0); // ждет пока можно будет слать, надо переделать
    while (stopServer == 0)
    {
        if (sendto(brcastSock, brMsg, sizeof(brMsg), 0, (struct sockaddr *) &brcastAddr, servAddrSize) == -1) {
            perror("can`t send msg type 1");
            exit(EXIT_FAILURE);
        }
        printf("Send msg type 2\n");
        sleep(GOT_MSG_DELAY);
        while(stop2 != 0 && stopServer == 0);
    }

    //char *stopMsg = "STOP";
    strcpy(brMsg, "STP");
    if (sendto(brcastSock, brMsg, sizeof(brMsg), 0, (struct sockaddr *) &brcastAddr, servAddrSize) == -1) {
        perror("can`t send msg type 2");
        exit(EXIT_FAILURE);
    }
    printf("Send STOP msg to clients type 2\n");

    pthread_exit(NULL);
}

void *GetMsg(void *arg) {
    int *clientSock = (int *) arg;
    int recvBuf[2];
    char *strBuf;
    struct Msg msg;

    recv(*clientSock, &recvBuf, sizeof(recvBuf), 0);   // recv header
    strBuf = malloc(sizeof(char) * recvBuf[1]);
    if (recv(*clientSock, strBuf, sizeof(char) * recvBuf[1], 0) == -1)        // recv random string
    {
        perror("Can`t recv string");
        exit(EXIT_FAILURE);
    }
    printf("time: %i, leng: %i, string: %s\n", recvBuf[0], recvBuf[1], strBuf);

    msg.waitTime = recvBuf[0];
    msg.strLeng = recvBuf[1];
    msg.str = strBuf;

    //pthread_mutex_lock(&lockAdd);
    AddInQueue(&msg);
    //pthread_mutex_unlock(&lockAdd);

    //pthread_exit(NULL);
}

void *Getter(void *arg)
{
    //int listenSock, tcpClientSock1;
    struct sockaddr_in servSockAddr;
    //pthread_t clientThreads1[QUEUE_MAX];
    signal(SIGUSR1, StopThread);

    listenSock1 = socket(AF_INET, SOCK_STREAM, 0);

    servSockAddr.sin_family = AF_INET;
    servSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servSockAddr.sin_port = htons(3009);
    unsigned int listenAddrSize = sizeof(servSockAddr);

    if (setsockopt(listenSock1, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    bind(listenSock1, (struct sockaddr *) &servSockAddr, listenAddrSize);
    listen(listenSock1, 3);

    while(stopServer == 0)
    {
        while(stop1 == 1); // чтобы не терять сообщения, ждем пока освободится место
        tcpClientSock1 = accept(listenSock1, (struct sockaddr *) &servSockAddr, &listenAddrSize);

        /*
        if (pthread_create(&clientThreads1[0], NULL, GetMsg, &tcpClientSock1) != 0) {
            perror("Can`t create thread");
            exit(EXIT_FAILURE);
        }

        pthread_detach(clientThreads1[0]);
         */
        GetMsg(&tcpClientSock1);
    }

    close(listenSock1);
    pthread_exit(NULL);
}

void *Sender(void *arg)
{
    //int listenSock, tcpClientSock2;
    int retVal;
    struct sockaddr_in servSockAddr;
    //pthread_t clientThreads1[QUEUE_MAX];
    struct Msg msg;
    signal(SIGUSR1, StopThread);

    listenSock2 = socket(AF_INET, SOCK_STREAM, 0);

    servSockAddr.sin_family = AF_INET;
    servSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servSockAddr.sin_port = htons(3011);
    unsigned int listenAddrSize = sizeof(servSockAddr);

    if (setsockopt(listenSock2, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    retVal = bind(listenSock2, (struct sockaddr *) &servSockAddr, listenAddrSize);
    if(retVal == -1)
    {
	perror("can`t bind");
        exit(EXIT_FAILURE);
    }


    retVal = listen(listenSock2, 3);
    if(retVal == -1)
    {
	perror("can`t listen");
        exit(EXIT_FAILURE);
    }


    while(stopServer == 0)
    {
        while(stop2 == 1); // ждем, пока в очереди появятся сообщения
        tcpClientSock2 = accept(listenSock2, (struct sockaddr *) &servSockAddr, &listenAddrSize);

        //pthread_mutex_lock(&lockGet);
        TakeFromQueue(&msg);
        //pthread_mutex_unlock(&lockGet);

        if (send(tcpClientSock2, &msg, sizeof(int) * 2, 0) == -1) // отправляем только первые 2 поля
        {
            perror("can`t send");
            exit(EXIT_FAILURE);
        }

        if (send(tcpClientSock2, msg.str, msg.strLeng, 0) == -1) {
            perror("can`t send");
            exit(EXIT_FAILURE);
        }
	close(tcpClientSock2);
    }
    close(listenSock2);
    pthread_exit(NULL);
}

void *GetKey(void *arg)
{
    char key;

    while (key != 's')
        scanf("%c", &key);

    stopServer = 1;
    pthread_exit(NULL);
}

int main() {
    printf("!!! SERVER !!!\n");

    if (pthread_mutex_init(&lockAdd, NULL) != 0)
    {
        printf("\n mutex init has failed\n");
        exit(EXIT_FAILURE);
    }

    if (pthread_mutex_init(&lockGet, NULL) != 0)
    {
        printf("\n mutex init has failed\n");
        exit(EXIT_FAILURE);
    }

    pthread_t controlThread;
    pthread_t msgRcvThread;
    pthread_t msgSndThread;
    pthread_t brcastThread[2];

    ShowQueue();

    if (pthread_create(&controlThread, NULL,  GetKey, NULL) != 0) {
        perror("Can`t create thread");
        exit(EXIT_FAILURE);
    }

    if (pthread_create(&brcastThread[0], NULL, SendWaitMsg, NULL) != 0) {
        perror("Can`t create thread");
        exit(EXIT_FAILURE);
    }

    if (pthread_create(&brcastThread[1], NULL, SendGotMsg, NULL) != 0) {
        perror("Can`t create thread");
        exit(EXIT_FAILURE);
    }

    if (pthread_create(&msgRcvThread, NULL, Getter, NULL) != 0) {
        perror("Can`t create thread");
        exit(EXIT_FAILURE);
    }

    if (pthread_create(&msgSndThread, NULL, Sender, NULL) != 0) {
        perror("Can`t create thread");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 2; ++i) // ждем броадкастеров и подтираем за ними
    {
        if (pthread_join(brcastThread[i], NULL) != 0) {
            perror("Can`t join br thread");
            return EXIT_FAILURE;
        }
    }

    if (pthread_join(controlThread, NULL) != 0) {
        perror("Can`t join br thread");
        return EXIT_FAILURE;
    }

    pthread_kill(msgRcvThread, SIGUSR1);
    pthread_kill(msgSndThread, SIGUSR1);

    if (pthread_join(msgRcvThread, NULL) != 0) {
        perror("Can`t join br thread");
        return EXIT_FAILURE;
    }

    if (pthread_join(msgSndThread, NULL) != 0) {
        perror("Can`t join br thread");
        return EXIT_FAILURE;
    }

    pthread_mutex_destroy(&lockAdd);
    pthread_mutex_destroy(&lockGet);

    close(listenSock1);
    close(listenSock2);
    close(tcpClientSock1);
    close(tcpClientSock2);

    return 0;
}

