#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>

int Get_Count_of_Numbers(int num)
{
    int counter = 1;

    while(num != 0)
    {
        num /= 10;
        counter++;
    }

    return counter; // +1 тк последний остаток в цикле не учтен
}

int Read_int(int mine)
{
    struct stat fileStat;
    int fileData;
    unsigned long arr_size;
    char *buf;

    lseek(mine, 0, SEEK_SET);
    stat("mine.txt", &fileStat);
    arr_size = fileStat.st_size - 1; //\n at the end
    buf = (char *)malloc(arr_size * sizeof (char));

    for (int i = 0; i < arr_size; i++) {
       read(mine, &buf[i], 1);
    }

    fileData = atoi(buf);

    return fileData;
}

void Rewrite_int(int mine, int output)
{
    unsigned long arr_size;
    char *buf;

    lseek(mine, 0, SEEK_SET);
    ftruncate(mine, 0);
    arr_size = Get_Count_of_Numbers(output);

    buf = (char *)malloc(arr_size * sizeof (char));;
    sprintf(buf, "%i", output);

    for (int i = 0; i < arr_size; i++) {
        write(mine, &buf[i], 1);
    }
}

void Go_to_Mine()
{
    struct flock lock;
    int mine = open("mine.txt", O_RDWR);
    int buf;

    lock.l_start = 0;
    lock.l_whence = SEEK_SET;
    lock.l_len = 0;
    lock.l_type = F_WRLCK;
    lock.l_pid = getpid();

    srand(time(0));

    printf("___i`m daughter, my pid is %d\n", getpid());

    do
    {
        if(fcntl(mine, F_SETLKW, &lock) == 0)
            printf("file locked by (%i)\n", getpid());

        buf = Read_int(mine);
        if (buf <= 0)// if gold is gone while it wait
        {
            lock.l_type = F_UNLCK;
            fcntl(mine, F_SETLK, &lock);
            printf("'EXIT' file unlocked by (%i)\n\n", getpid());
            break;
        }

        printf("gold value is: %i\n", buf);

        buf--;
        Rewrite_int(mine, buf);
        printf("take 1 gold (%i)\n", getpid());

        printf("gotta unlock the file (%i)\n", getpid());
        lock.l_type = F_UNLCK;
        fcntl(mine, F_SETLK, &lock);
        printf("file unlocked by (%i)\n\n", getpid());
        lock.l_type = F_WRLCK;
        sleep(1 + rand() % 4);

    } while (buf > 0);

    printf("mine is empty (%i)\n", getpid());
    close(mine);
}


int main(int argc, char *argv[])
{
    int num_of_prcs = atoi(argv[1]);
    pid_t pid[num_of_prcs];
    int status;
    char choise;

    printf("wanna use execl()? y/n\n");
    scanf("%c", &choise);

    for (int i = 0; i < num_of_prcs; i++) {
        pid[i] = fork();

        if (pid[i] == 0)
        {
           if(choise == 'y')
               execl("./unit_prog", NULL);
           else
               Go_to_Mine();

           printf("EXIT(%i)\n", getpid());
           exit(EXIT_SUCCESS);
        }
    }

    for (int i = 0; i < num_of_prcs; i++) {
        waitpid(pid[i], &status, 0);
    }

    return 0;
}
