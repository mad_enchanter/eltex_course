#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define MAX_TARGETS 15
#define MAX_UNITS 3

struct Unit
{
    int x;
    int y;
    int mov_dir;
};

int ChooseDir(int x, int y, int dim)
{
    int up, down, left, right;

    up = dim - y;
    down = y;
    right = dim - x;
    left = x;

    if(up > down)
    {
        if(up > left)
        {
            if(up > right)
                return 1; // up
            else
                return 4; // right
        }

        else
        {
            if(left > right)
                return 3; // left
            else
                return 4;
        }
    }

    else
    {
        if(down > left)
        {
            if(down > right)
                return 2; // down

            else
                return 4; // right
        }

        else
        {
            if(left > right)
                return 3; // left

            else
                return 4;
        }
    }
}

void ClearMap(int dim, int map[dim][dim])
{
    for (int i = 0; i < dim; i++) {
        for (int j = 0; j < dim; j++) {
            map[i][j] = 0;
        }
    }
}

void UpdateMap(struct Unit units[MAX_UNITS][2], int targets[MAX_TARGETS][2], int dim, int map[dim][dim])
{
    for (int i = 0; i < MAX_TARGETS; i++)
    {
        map[targets[i][0]][targets[i][1]] = 9;
    }

    for (int i = 0; i < MAX_UNITS; i++)
    {
        map[units[i]->x][units[i]->y] = i + 1;
    }
}

void PrintMap(int dim, int map[dim][dim])
{
    for (int i = 0; i < dim; i++) {
        for (int j = 0; j < dim; j++) {

            if(map[j][i] == 0)
                printf("| . "); // empty

            else if(map[j][i] == 9)
                printf("| $ "); // target

            else
                 printf("| %i ", map[j][i]); // unit
        }
        printf("|\n");
    }
}

void Move(struct Unit *unit)
{
    int x_mov, y_mov;

    switch (unit->mov_dir) {
    case 1:
        y_mov = 1;
        x_mov = 0;
        break;

    case 2:
        y_mov = -1;
        x_mov = 0;
        break;

    case 3:
        y_mov = 0;
        x_mov = -1;
        break;

    case 4:
        y_mov = 0;
        x_mov = 1;
        break;
    }

    unit->x +=x_mov;
    unit->y +=y_mov;
}

void ToScout(struct Unit *unit, int targets[MAX_TARGETS][2], int *counter)
{
    for (int i = 0; i < MAX_TARGETS; i++) {
        if(unit->x == targets[i][0] && unit->y == targets[i][1])
        {
            printf("got it\n");
            (*counter)++;
        }
    }
}

int main(int argc, char *argv[])
{
    int num_of_prcs = atoi(argv[1]);
    pid_t pid[num_of_prcs];
    int status;
    int dim = atoi(argv[2]);
    int map[dim][dim];
    int counters[MAX_UNITS];
    struct Unit units[MAX_UNITS][2];
    int targets[MAX_TARGETS][2];

    srand(time(0));

    if(argc < 2)
    {
        printf("invalid number of args\n");
        exit(EXIT_FAILURE);
    }

    // START
    for (int i = 0; i < MAX_TARGETS; i++)
    {
        targets[i][0] = (rand() % 9);
        targets[i][1] = (rand() % 9);
    }

    for (int i = 0; i < MAX_UNITS; i++)
    {
        units[i]->x = (rand() % 9);
        units[i]->y = (rand() % 9);
        units[i]->mov_dir = ChooseDir(units[i]->x, units[i]->y, dim);
        counters[i] = 0;
    }
    //

    ClearMap(dim, map);
    UpdateMap(units, targets, dim, map);
    PrintMap(dim, map);

    int fd[num_of_prcs][2];

    for (int i = 0; i < num_of_prcs; i++) {
        pipe(fd[i]);
        pid[i] = fork();

        if (pid[i] == 0)
        {
            sleep(1);
            int targCount = 0;
            int echo[2];
            close(fd[i][0]);

            do
            {
                sleep(2);
                Move(units[i]);
                ToScout(units[i], targets, &targCount);
                echo[0] = units[i]->x;
                echo[1] = units[i]->y;
                write(fd[i][1], echo, sizeof (int[2]));

            } while(units[i]->x < 9 && units[i]->x > 0 &&
                   units[i]->y < 9 && units[i]->y > 0);

            echo[0] = 11;
            echo[1] = targCount;
            write(fd[i][1], &echo, sizeof (echo));

            exit(EXIT_SUCCESS);
         }

        close(fd[i][1]);
    }

    int compl_prcs = 0, i, buf[2];

    while(compl_prcs != num_of_prcs)
    {
        compl_prcs = 0;
        for (i = 0; i < num_of_prcs; i++)
        {
            sleep(1);
            if(read(fd[i][0], buf, sizeof (buf)) > 0)
            {
                if(buf[0] != 11)
                {
                    units[i]->x = buf[0];
                    units[i]->y = buf[1];
                }

                else
                {
                    counters[i] = buf[1];
                    compl_prcs++;
                }
            }

            else
                compl_prcs++;

            system("clear");
            ClearMap(dim, map);
            UpdateMap(units, targets, dim, map);
            PrintMap(dim, map);
            for (int j = 0; j < MAX_UNITS; j++)
                printf("unit %i) %i\n", j + 1, counters[j]);
        }
    }

    return 0;
}
