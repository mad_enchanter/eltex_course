#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>



struct Balloon
{
    int x;
    int y;
    int dir;
};

struct msg_buf
{
  long type;
  int bal_num;
  struct Balloon bal_coord;
};


void ClearMap(int dim, int map[dim][dim])
{
    for (int i = 0; i < dim; i++) {
        for (int j = 0; j < dim; j++) {
            map[i][j] = 0;
        }
    }
}

void UpdateMap(int numOfPrcs, struct Balloon balloon[numOfPrcs], int dim, int map[dim][dim])
{
    int i,j;
    for (int i = 0; i < numOfPrcs; i++)
    {
        if(balloon[i].x >= 0 && balloon[i].y >= 0)
            map[balloon[i].x][balloon[i].y] = 1 + i;
    }
}

void PrintMap(int dim, int map[dim][dim])
{
    for (int i = 0; i < dim; i++) {
        for (int j = 0; j < dim; j++) {

            if(map[j][i] == 0)
                printf("| . "); // empty

            else
                 printf("| %i ", map[j][i]); // unit
        }
        printf("|\n");
    }
}

int CheckBorders(struct Balloon *balloon, int dim)
{
    if(balloon->x >= dim || balloon->x < 0
            || balloon->y >= dim || balloon->y < 0)
        return 1;

    else return 0;
}

int GetDir()
{
    return (rand() % 4) + 1;
}

void MoveBallon(struct Balloon *balloon)
{
    //int dir = GetDir();

    switch (balloon->dir) {

    case 1:
        balloon->y++;
        break;

    case 2:
        balloon->y--;
        break;

    case 3:
        balloon->x++;
        break;

    case 4:
        balloon->x--;
        break;
    }
}

int main(int argc, char *argv[])
{
    int i, status, dim = 7, numOfPrcs;
    int map[dim][dim];
    key_t key;
    
    if(argc < 2)
    {
        printf("invalid number of args\n");
        exit(EXIT_FAILURE);
    }
    
    numOfPrcs = atoi(argv[1]);
    struct Balloon balloon[numOfPrcs];
    pid_t pid[numOfPrcs];

    srand(getpid());

    // generating the key
    if((key = ftok(".", 8)) == -1)
    {
        perror(" Can`t create key");
        exit(EXIT_FAILURE);
    }

    // set start position for each balloon
    for (i = 0; i < numOfPrcs; i++)
    {
        balloon[i].x = rand() % (dim - 1);
        balloon[i].y = rand() % (dim - 1);
        balloon[i].dir = GetDir();
    }

    // init MSG_QUEUE
    int msgqid;
    struct msg_buf msg_buf;
    msg_buf.type = 1;

    if((msgqid = msgget(key, IPC_CREAT | IPC_EXCL | 0660)) == -1) // IPC_CREAT | IPC_EXCL | 0660
    {
        perror(" Can`t create msg queue ");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < numOfPrcs; i++)
    {
        pid[i] = fork();
        if(pid[i] == 0)
        {
            msg_buf.bal_num = i; // send balloon num
            while(CheckBorders(&balloon[i], dim) == 0)
            {
                // sending msg
                msg_buf.bal_coord = balloon[i]; // send balloon coord
                if(msgsnd(msgqid, &msg_buf, sizeof(msg_buf), 0) == -1)
                {
                    perror(" Can`t send in msg queue ");
                    exit(EXIT_FAILURE);
                }
                MoveBallon(&balloon[i]);
                sleep(1);
            }

            balloon[i].x = balloon[i].y = -1;
            msg_buf.bal_coord = balloon[i];
            if(msgsnd(msgqid, &msg_buf, sizeof(msg_buf), 0) == -1)
            {
                perror(" Can`t send in msg queue ");
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
    }

    int prcStoped = 0;

    while (prcStoped < numOfPrcs)
    {
        // recv msgs from queue
        if(msgrcv (msgqid, &msg_buf, sizeof(msg_buf), msg_buf.type, 0) == -1)
        {
            perror(" Can`t rcv from msg queue ");
            exit(EXIT_FAILURE);
        }
        balloon[msg_buf.bal_num].x = msg_buf.bal_coord.x;
        balloon[msg_buf.bal_num].y = msg_buf.bal_coord.y;

        if(balloon[msg_buf.bal_num].x == -1 && balloon[msg_buf.bal_num].y == -1)
            prcStoped++;

        system("clear");
        ClearMap(dim, map);
        UpdateMap(numOfPrcs, balloon, dim, map);
        PrintMap(dim, map);

        sleep(1);
    }

    printf("Done!\n");

    // END MSG_QUEUE
    if(msgctl(msgqid, IPC_RMID, NULL) == -1)
    {
        perror(" Can`t delete msg queue ");
        exit(EXIT_FAILURE);
    }

    return 0;
}
