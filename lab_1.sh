#!/bin/bash

dir_name="dir_";
file_name="file_";

dir_max=5;
subdir_max=10;
file_max=20;

set_new_config() # задать шаблоны и кол-во папок и файлов
{
 echo "Enter directory name template: ";
 read dir_name;

 echo "Enter file name template: ";
 read file_name;

 echo "Enter number of directories: ";
 read dir_max;

 echo "Enter number of subdirectories: ";
 read subdir_max;

 echo "Enter number of files: ";
 read file_max;
}

create() 
{
 case $2 in
  0) 
     for (( i=0; i<$dir_max; i++ )); 
     do
      mkdir "$1/$dir_name$i";
      create "$1/$dir_name$i" `expr $2 + 1`;
     done;;

  1) 
     for (( j=0; j<$subdir_max; j++ )); 
     do
      mkdir "$1/$dir_name$j";
      create "$1/$dir_name$j" `expr $2 + 1`;
     done;;

  2) 
     for (( k=0; k<$file_max; k++ )); 
     do
      touch "$1/$file_name$k";
     done;;
 esac
}



if [ $# -gt 0 ] 
then 

echo "Use custom settings? y or n";
read choose;

while [[ $choose != "y" && $choose != "n" ]] # пока не получили нормальный ответ
do
 echo "Wrong input";
 read choose;
done

if [ $choose = "y" ]
then
 set_new_config;
fi

create $1 0;

else
 echo "You forget to enter the path";

fi

exit 0
