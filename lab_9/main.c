#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <signal.h>

void Exit(int sig)
{
    printf("stop %i\n", getpid());
    exit(EXIT_SUCCESS);
}


int main()
{
    srand(time(0));

    key_t key;
    int i, j, shmid, *status, counter = 0, numOfPrcs = 3;
    pid_t pid[numOfPrcs], parentPid = getpid();
    int *virtaddr, *s;

    // generating the key
    if((key = ftok(".", 8)) == -1)
    {
        perror(" Can`t create key");
        exit(EXIT_FAILURE);
    }


    // SHM
    if((shmid = shmget(key, sizeof (int) * numOfPrcs, IPC_CREAT | IPC_EXCL | 0660)) == -1) //IPC_CREAT | IPC_EXCL | 0660
    {
        perror(" Can`t create shmem  ");
        exit(EXIT_FAILURE);
    }

    if((virtaddr = shmat(shmid, NULL, 0)) == -1)
    {
        perror(" Can`t connect shmem  ");
        exit(EXIT_FAILURE);
    }

    // SEM
    int semid;

    if((semid = semget(key, 1, IPC_CREAT | IPC_EXCL | 0660)) == -1) // IPC_CREAT | IPC_EXCL | 0660
    {
         perror(" Can`t create sem ");
         exit(EXIT_FAILURE);
    }

    // set sem value = 1
    if(semctl(semid, 0, SETVAL, 1) == -1)
    {
         perror(" Can`t set sem value ");
         exit(EXIT_FAILURE);
    }

    signal(SIGUSR1, Exit);

    for (i = 0; i < numOfPrcs; i++)
    {
        pid[i] = fork();
        if(pid[i] == 0)
            break;
    }

     s = virtaddr;
    if(getpid() == parentPid)
    {
        for (i = 0; i < numOfPrcs; i++)
             printf("%i ", pid[i]);
        printf("\n");

        // writing in shared memory
        for (i = 0; i < numOfPrcs; i++)
           s[i] = pid[i];
    }

    else sleep(1);

    int choose = 0;

    for (i = 0; i < numOfPrcs; i++)
    {
        if(pid[i] == 0)
        {
            struct sembuf sem_buf;
            sem_buf.sem_num = 0;
            sem_buf.sem_flg = 0;

            int temp;

            while(counter < numOfPrcs)
            {
                temp = semctl(semid, 0, GETVAL, 0);
                sem_buf.sem_op = -1;

                semop(semid, &sem_buf, 1);

                if(s[i] == -1) // if prc was killed
                {
                    printf("stop (%i)\n",(int)getpid());
                    sem_buf.sem_op = 1;
                    semop(semid, &sem_buf, 1);
                    exit(EXIT_SUCCESS);
                }

                printf("%i locked area\n", getpid());
                counter = 0;

                do {
                    choose = rand() % numOfPrcs;
                } while(s[choose] == getpid() || s[choose] == -1);

                printf("%i killed by(%i)\n", s[choose], getpid());
                kill(s[choose], SIGUSR1);
                s[choose] = -1;

                for (i = 0; i < numOfPrcs; i++) // check all stopped prcs
                {
                    if(s[i] == -1)
                        counter++;
                }

                sleep(1);
                printf("%i unlocked area\n", getpid());
                sem_buf.sem_op = 1;
                semop(semid, &sem_buf, 1);
            }
            exit(EXIT_SUCCESS);
        }
    }

    counter = 0;
    int winnerPid;
    while(counter < numOfPrcs - 1)
    {
        counter = 0;

        for (i = 0; i < numOfPrcs; i++) // check all stopped prcs
        {
            if(s[i] == -1)
                counter++;

            else winnerPid = s[i];

             printf("%i ", s[i]);
        }
        printf("\n");
        sleep(1);
    }

    printf("winner is %i\n", winnerPid);

    // END SHMEM
    shmdt(virtaddr);
    if(shmctl(shmid, IPC_RMID, 0) < 0)
    {
        printf("can`t delete shmem");
        exit(1);
    }

    // END SEM
    if(semctl(semid, 0, IPC_RMID, 0) == -1)
    {
        perror(" Can`t delete sems ");
        exit(EXIT_FAILURE);
    }

    printf("Done\n");
    return 0;
}

