#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>

pthread_mutex_t lock;

void ChangeHoneyVal(int *honeyuf, int val)
{
    pthread_mutex_lock(&lock);
    *honeyuf += val;
    pthread_mutex_unlock(&lock);
}

void *ExtractHoney(void *arg)
{
    int *honeyBuf = (int *) arg;
    while(1)
    {
        sleep((rand() % 3) + 1);
        ChangeHoneyVal(honeyBuf, 10);
        printf("Bee add 10 honey, now its %i\n", *honeyBuf);
    }
}

int main(int argc, char *argv[])
{
    int i, honeyBuf = 100;

    if(argc < 2)
    {
        printf("invalid number of args\n");
        exit(EXIT_FAILURE);
    }

    int numOfBees = atoi(argv[1]);
    pthread_t beeThreads[numOfBees];


    if (pthread_mutex_init(&lock, NULL) != 0)
    {
         printf("\n mutex init has failed\n");
         return 1;
    }

    //make threads
    for (i = 0; i < numOfBees; i++)
    {
        if (pthread_create(&beeThreads[i], NULL, ExtractHoney, &honeyBuf) != 0)
        {
                perror("Can`t create thread");
                exit(EXIT_FAILURE);
        }
    }

    while(1)
    {
        sleep(2);
        if(honeyBuf < 50) // if honey nit enough
        {
            printf("Winnie is waiting because honey is not enough, now its %i\n", honeyBuf);
            sleep(2);
            if(honeyBuf < 50) // winnie in the hospital
                break;
        }

        ChangeHoneyVal(&honeyBuf, -50);
        printf("Winnie take 50 honey, now its %i\n", honeyBuf);
    }

    for (i = 0; i < numOfBees; i++)
        pthread_cancel(beeThreads[i]);

    for (i = 0; i < numOfBees; i++)
    {
        if (pthread_join(beeThreads[i], NULL) != 0)
        {
            perror("Can`t join thread");
            return EXIT_FAILURE;
        }
    }
    pthread_mutex_destroy(&lock);

    printf("Winnie went to hospital because of starvation\n");
    return 0;
}
