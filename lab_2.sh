#!/bin/bash

setAlarm()
{

echo -n "Enter month: ";
read month;
while [[ $month -lt 0 || $month -gt 12 ]]; do
	echo -n "Invalid value, try again(0 - 12): ";
	read month;
done;

echo -n "Enter day: "
read day
while [ $day -lt 0 -o $day -gt 31 ]; do
	echo -n "Invalid value, try again(0 - 31): "
	read day
done 

echo -n "Enter hour: "
read hour
while [ $hour -lt 0 -o $hour -gt 23 ]; do
	echo -n "Invalid value, try again(0 - 23): "
	read hour
done 

echo -n "Enter minute: "
read minute
while [ $minute -lt 0 -o $minute -gt 59 ]; do
	echo -n "Invalid value, try again(0 - 59): "
	read minute
done 


echo -n "Enter path to sound file: ";
#read sound_file
sound_file="/home/krioldar/relax.mp3";
while [ ! -f "$sound_file" ]
do
	echo -n "file not found, try again: "
	read sound_file
done 

echo "$minute $hour $day $month * export DISPLAY=:0.0; gnome-terminal -x mpg123 -C -y $sound_file";
}



addAlarm()
{
setAlarm;
#write out current crontab
crontab -l > mycron
#echo new cron into cron file
echo -n "$minute $hour $day $month * export DISPLAY=:0.0; gnome-terminal -x mpg123 -C -y $sound_file " >> mycron;
#echo "#al" >> mycron;
#install new cron file
crontab mycron
rm mycron
}


editAlarm()
{
showAlarms;

echo "which one wanna edit";
read choice

setAlarm;
echo $alarmInfo

i=1
while read -r line && [ $i -lt $choice ]; do 
	((i++));
done < mycron;

str="$minute $hour $day $month * export DISPLAY=:0.0; gnome-terminal -x mpg123 -C -y $sound_file";

sed -e "$choice"s!^.*!"$str"! mycron > tempcron
mv tempcron mycron 
crontab mycron
rm mycron

#######################    sed -e 3s/[0-9].*/"plz"/ file 
}


removeAlarm()
{
showAlarms;

echo "which one wanna delete?";
read choise

sed "$choise"d mycron > tempcron
mv tempcron mycron
crontab mycron
rm mycron
}


showAlarms()
{
crontab -l | grep "^[0-9].*mpg123.*" > mycron

i=1;
echo "№ min hour mon year";
while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "$i: $line" | sed -r "s/\*.+//";
done < mycron
rm mycron
}


#MAIN
echo -e " 1 - Add alarm\n 2 - Show alarms\n 3 - Delete alarm\n 4 - Edit alarm\n 5 - Exit";
read menuChoice
while [ $menuChoice -ne 5 ]; do
	clear;
	case $menuChoice in
	 1) addAlarm;;
	 2) showAlarms;;
	 3) removeAlarm;;
	 4) editAlarm;;
	 *) echo -e "Invalid value, try again";;
	esac
echo -e "\n 1 - Add alarm\n 2 - Show alarms\n 3 - Delete alarm\n 4 - Edit alarm\n 5 - Exit";
read menuChoice
done
exit 0	


