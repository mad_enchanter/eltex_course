#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>


int main(int argc, char const *argv[])
{
    int i, ListenSocket, ClientSocket, msg_size;
    struct sockaddr_in ServerAddr;
    int recvbuf;
    int sendbuf[6];
    pid_t pid[3];
    int mat[3][3] = {{3,2,1}, {4,5,6}, {7,8,1}};
    int vec[3] = {3, 1, 7};
    int result[3], count = 0;
    char curDir[100];
	
    printf("multiply mat:\n");
    
    for(i = 0; i < 3; i++)
	printf("%i %i %i\n", mat[i][0], mat[i][1], mat[i][2]);	

    printf("\nby vec: \n%i %i %i\n\n", vec[0], vec[1], vec[2]);

    // Create a SOCKET for connecting to server
    ListenSocket = socket(AF_INET, SOCK_STREAM, 0);

    // Setup the TCP listening socket
    ServerAddr.sin_family = AF_INET;
    ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    ServerAddr.sin_port = htons(2006);

    
    if (chdir(get_current_dir_name()) == -1)
    {
   	perror("");
    	exit(EXIT_FAILURE);
    }

    for(i = 0; i < 3; i++)
    {
	pid[i] = fork();
	if(pid[i] == 0)
	   {
		sleep(2);
		execl("client", "client", (char *)NULL);
	   }
    }	

    if (bind(ListenSocket, (struct sockaddr *)&ServerAddr, sizeof(ServerAddr)) == -1)
    {
        perror("Can`t bind socket");
        close(ListenSocket);
        exit(EXIT_FAILURE);
    }

    if (listen(ListenSocket, 50) == -1)
    {
        printf("Can`t listen socket\n");
        close(ListenSocket);
        exit(EXIT_FAILURE);
    }

    while (count < 3)
    {
        // Accept a client socket
        socklen_t addr_size = sizeof(ServerAddr);
        if((ClientSocket = accept(ListenSocket, (struct sockaddr *)&ServerAddr, &addr_size)) == -1)
        {
            perror("accept failed");
            close(ClientSocket);
            exit(EXIT_FAILURE);
        }

        printf("got connection\n");

        // send mat row
        for (i = 0; i < 3; i++)
        {
            sendbuf[i] = htonl(mat[count][i]);
            sendbuf[i + 3] = htonl(vec[i]);
        }

       	int size;
	if((size = send(ClientSocket, sendbuf, sizeof(int) * 6, 0)) == -1)
    	{
       	    perror("send failed");
       	    close(ClientSocket);
    	    exit(EXIT_FAILURE);
   	}

        printf("Sent row: (%i %i %i)\n", mat[count][0], mat[count][1], mat[count][2]);
        printf("Sent vec: (%i %i %i)\n", vec[0], vec[1], vec[2]);

        if((msg_size = recv(ClientSocket, &recvbuf, sizeof(recvbuf), 0)) == -1)
        {
            perror("recv failed");
            close(ClientSocket);
            exit(EXIT_FAILURE);
        }

        if (msg_size > 0)
        {
            printf("Received msg: %i\n", recvbuf);
            result[count] = recvbuf;
            count++;
        }
    }

    // shutdown the connection since we're done
    close(ClientSocket);

    printf("Result: (%i %i %i)\n", result[0], result[1], result[2]);
    return 0;
}
