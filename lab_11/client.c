#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <netinet/in.h>



int main(int argc, char const *argv[])
{
    int ConnectSocket;
    struct sockaddr_in ServerAddr;
    int msg_size, i;
    int recvbuf[6];
    int sendbuf = 0;
    // Connect to server
    ConnectSocket = socket(AF_INET, SOCK_STREAM, 0);

    // Setup the TCP listening socket
    ServerAddr.sin_family = AF_INET;
    ServerAddr.sin_addr.s_addr = inet_addr("192.168.0.103");
    ServerAddr.sin_port = htons(2006);

    if (connect(ConnectSocket, (struct sockaddr*)&ServerAddr, sizeof(ServerAddr)) == -1)
    {
        perror("Can`t connect to server");
        close(ConnectSocket);
        exit(EXIT_FAILURE);
    }

    // rcv result
    if((msg_size = recv(ConnectSocket, recvbuf, sizeof(int) * 6, 0)) == -1)
    {
        perror("recv failed");
        close(ConnectSocket);
        exit(EXIT_FAILURE);
    }

    if (msg_size > 0)
    {
        printf("Recieved msg: ");
        for (i = 0; i < 6; i++)
             printf("%i ", ntohl(recvbuf[i]));
        printf("\n");

        for (i = 0; i < 3; i++)
            sendbuf += (ntohl(recvbuf[i]) * ntohl(recvbuf[i + 3]));
    }

    else if (msg_size == 0)
        printf("Connection closing...\n");

    // send result on server
    if(send(ConnectSocket, &sendbuf, sizeof (sendbuf), 0) == -1)
    {
        perror("send failed");
        close(ConnectSocket);
        exit(EXIT_FAILURE);
    }
    printf("Sent: %i\n", sendbuf);

    // shutdown the connection since we're done
    close(ConnectSocket);
    return 0;
}
